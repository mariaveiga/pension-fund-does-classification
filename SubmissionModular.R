source('Modular.R')

model <- train.model(dat.orig,sigma=2,C=350)

validation <- read.csv(file="validation.csv", head=FALSE, sep=",", col.names=1:27)

result <- predict.from.model(model, validation)
write.csv(as.numeric(as.character(result)), file="pension2.csv", row.names=FALSE)
